# aesop latex
Projeto com o objetivo de escrever as fábulas compiladas pelo [Lit2Go](http://etc.usf.edu/lit2go/35/aesops-fables/) em um arquivo para, futuramente, ser lido com os áudios em inglês - da Lit2Go também - para auxiliar no estudo da língua.

## Leitura
Já é possível ler os textos agrupados no [pdf](https://gitlab.com/felipedacs/aesop-latex/raw/master/aesop.pdf?inline=false) e para baixá-lo no Kindle é necessário enviar um e-mail ao seu dispositivo com o assunto em branco e o arquivo em anexo.
